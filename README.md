**Instructions to use my project**

1.Open your terminal window and navigate to the top level of your local repository.
$ cd ~/repos/bitbucketstationlocations/TravelSite
2.Enter the git pull --all command to pull all the changes from Bitbucket. 
3.Enter your Bitbucket password when asked for it.
4.The git pull command merges the file/folder from your remote repository (Bitbucket) into your local repository with a single command.
5.Navigate to your repository folder on your local system and you'll see the file/folder you just added.
6.You can run the .html files in any of the web browsers!Just double click on index.html, it will open the website in your default browser.
7. Alternatively you can open the file in Notepad/Text editors and run the file from there,on any browsers from there.
**or** 

cd existing-project
git init
git add --all
git commit -m "Initial Commit"
git remote add origin http://<Bitbucket Server URL>/scm/tis/website.git
git push -u origin master

**Note : Licence File**
I picked the MIT licence as it seemed like a good option for providing open source licencing to my project. I got the licence content from https://choosealicense.com/licenses/mit/.



